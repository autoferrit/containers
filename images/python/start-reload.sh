#!/usr/bin/env sh

set -ex

export env="dev"

uvicorn \
  --reload \
  --host "${server_host:-localhost}" \
  --port "${server_port:-5000}" \
  --log-level "${log_level:-DEBUG}" \
  "${app_module:-'src/bo/entrypoints/fastapi_app:app'}"
